package be.dastudios.teamwombat.services.impl;

import be.dastudios.teamwombat.model.adventurer.Adventurer;
import be.dastudios.teamwombat.model.adventurer.Gender;
import be.dastudios.teamwombat.model.classes.CharacterClass;
import be.dastudios.teamwombat.model.races.Race;
import be.dastudios.teamwombat.services.CharacterService;
import be.dastudios.teamwombat.util.KeyboardUtility;
import be.dastudios.teamwombat.util.LoadUtility;

import java.util.Arrays;
import java.util.List;

public class CharacterServiceImpl implements CharacterService {
    @Override
    public Adventurer createCharacter() {

        String nameAdventurer = KeyboardUtility.ask("What is your adventurer's name?");
        String gender = KeyboardUtility.ask(nameAdventurer + "...yes, it's a weak name. You will be forgotten quickly. Are you a male or female?");
        String race = "";

        List<String> races = Arrays.asList("Human", "Dwarf", "Elf");
        List<String> characterClass = Arrays.asList("Mage", "Ranger", "Warrior", "Rogue");

        if (gender.equalsIgnoreCase("male")) {
            race = KeyboardUtility.askForChoice("Can you tell me more about him? Choose between:", races);
        } else {
            race = KeyboardUtility.askForChoice("Can you tell me more about her?Choose between:", races);
        }
        String classNAME = KeyboardUtility.askForChoice("What is " + nameAdventurer + "'s class? Choose between:", characterClass);

        Adventurer adventurerOfUser = new Adventurer(nameAdventurer, Gender.valueOf(gender.toUpperCase()));
        adventurerOfUser.setRace(Race.getRaceByName(race));
        adventurerOfUser.setCharacterClass(CharacterClass.getCharacterClassByName(classNAME));

        showAdventurerInfo(adventurerOfUser);
        return adventurerOfUser;


    }

    private void showAdventurerInfo(Adventurer adventurer) {
        System.out.println("Your character is being created");
        LoadUtility.loadingDots();
        System.out.printf("\n%s the %s %s %s created.\n", adventurer.getName(),
                adventurer.getGender().getValue(),
                adventurer.getRace().getName(), adventurer.getCharacterClass().getClassName());
        System.out.println(adventurer);
        System.out.println("Map is being loaded");
        LoadUtility.loadingDots();
        System.out.println("\nMysterious Forest loaded");
    }
}
