package be.dastudios.teamwombat.services.impl;

import be.dastudios.teamwombat.services.CharacterService;
import be.dastudios.teamwombat.services.ManagerService;
import be.dastudios.teamwombat.services.MapService;
import be.dastudios.teamwombat.util.KeyboardUtility;
import be.dastudios.teamwombat.util.TextUtil;

import java.util.Arrays;
import java.util.List;

public class ManagerServiceImpl implements ManagerService {

    private List<String> options = Arrays.asList("New", "Load", "Reset", "Controls", "Settings", "Quit");
    private List<String> descriptions = Arrays.asList("Start a new game", "Load a saved game", "Reset saved games",
            "Show game controls", "Edit Settings", "Exit game");
    private MapService mapservice = new MapServiceImpl();
    private CharacterService characterService = new CharacterServiceImpl();

    @Override
    public void showMenu(String choose) {

        switch (choose.toLowerCase()) {
            case "new":
                startNewGame();
                break;
            case "load":
                loadSavedGame();
                break;
            case "controls":
                gameControlsInfo();
                break;
            case "settings":
                gameSettings();
                break;
            case "quit":
                quitGame();
                break;
            default:
                break;


        }
    }

    private void quitGame() {
    }

    private void gameSettings() {
    }

    private void gameControlsInfo() {
    }

    private void loadSavedGame() {
    }

    private void startNewGame() {
        String message = "What map do you want to play?";
        int chooseIndex = KeyboardUtility.askForChoice(message, mapservice.getGameMaps());
        if (chooseIndex == 1) {
            System.out.println("Coming soon...");
            startNewGame();
        } else {

            mapservice.createMap(characterService.createCharacter());


        }


    }

    @Override
    public void start() {

        TextUtil.printTitle("Legend of the Lamb |||");
        TextUtil.printSubheading("D.A. Studios Belgium");
        System.out.println("Loading maps and saved games.");
        loadingDots();
        String choose = KeyboardUtility.askForChoice(options, descriptions);
        showMenu(choose);


    }

    private void loadingDots() {

        for (int i = 0; i < 3; i++) {
            System.out.print(".");
            try {
                Thread.sleep(1000);

            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }
        System.out.println();

    }
}
