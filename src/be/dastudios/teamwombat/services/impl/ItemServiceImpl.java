package be.dastudios.teamwombat.services.impl;

import be.dastudios.teamwombat.model.adventurer.Adventurer;
import be.dastudios.teamwombat.model.items.Item;
import be.dastudios.teamwombat.model.items.LockPicks;
import be.dastudios.teamwombat.model.items.Potion;
import be.dastudios.teamwombat.services.ItemService;
import be.dastudios.teamwombat.util.KeyboardUtility;
import be.dastudios.teamwombat.util.RandomUtility;

import java.util.Arrays;
import java.util.List;


public class ItemServiceImpl implements ItemService {
   private static final List<Item> itemList = Arrays.asList(LockPicks.LOCKPICKS, Potion.MANA_POTION,
           Potion.HEALTH_POTION, Potion.STAMINA_POTION);



    @Override
    public int genItem(Adventurer adventurer) {
        boolean OpenOrNot = KeyboardUtility.askYOrN("Open ye old treasure chest?");
        if(OpenOrNot){
            Item item = itemList.get(RandomUtility.RANDOM_NUM_GEN.nextInt(itemList.size()));
            System.out.printf("Hooray you found one %s, put it to good use", item.getItemInfo());
            adventurer.getInventory().addItem(item);
            return 1;

        }else System.out.println("You didn't open the chest.You are an idiot, you don't know what your missing.");
        return 0;




    }
}
