package be.dastudios.teamwombat.services.impl;

import be.dastudios.teamwombat.model.adventurer.Adventurer;
import be.dastudios.teamwombat.model.enemies.EnemyGenerator;
import be.dastudios.teamwombat.model.maps.MapFactory;
import be.dastudios.teamwombat.model.maps.Tile;
import be.dastudios.teamwombat.services.ItemService;
import be.dastudios.teamwombat.services.MapService;
import be.dastudios.teamwombat.util.KeyboardUtility;
import java.util.List;
import java.util.Map;


public class MapServiceImpl implements MapService {
    private static MapFactory mapFactory = new MapFactory();
    private static EnemyGenerator enemyGenerator = new EnemyGenerator();
    private static final String EAST = "east";
    private static final String WEST = "west";
    private static final String NORTH = "north";
    private static final String SOUTH = "south";
    private static ItemService itemService = new ItemServiceImpl();


    @Override
    public String[] getGameMaps() {
        return mapFactory.getMaps();
    }

    @Override
    public void createMap(Adventurer adventurer) {
        Map<String, List<Tile>> map1 = mapFactory.createMap1();
        adventurer.setPlayerTile(new Tile(2, 5));

        startGame(adventurer, map1);


    }

    private void startGame(Adventurer adventurer, Map<String, List<Tile>> map1) {
        System.out.printf("Welcome %s to the Mysterious Forest, hell on earth", adventurer.getName());
        lookDirection(map1, adventurer);


    }

    private static void chooseMoveDirection(Map<String, List<Tile>> map,Adventurer adventurer) {
        String askToMove = "Which direction do you wanna go?";
        Tile movedTile = adventurer.getPlayerTile();
        String chosenDirection = KeyboardUtility.ask(askToMove);
        if (chosenDirection.toLowerCase().contains(NORTH)) {
            checkMoveableTiles(map,adventurer,checkTile(map,getNorthTile(adventurer.getPlayerTile())),0,1);
        } else if (chosenDirection.contains(SOUTH)) {
            checkMoveableTiles(map,adventurer,checkTile(map,getSouthTile(adventurer.getPlayerTile())),0,-1);
        } else if (chosenDirection.contains(EAST)) {
            checkMoveableTiles(map,adventurer,checkTile(map,getEastTile(adventurer.getPlayerTile())),1,0);
        } else if (chosenDirection.contains(WEST)) {
            checkMoveableTiles(map,adventurer,checkTile(map,getWestTile(adventurer.getPlayerTile())),-1,0);
        }
    }

    private static void lookDirection(Map<String, List<Tile>> map, Adventurer adventurer) {

        Tile north = getNorthTile(adventurer.getPlayerTile());
        Tile south = getSouthTile(adventurer.getPlayerTile());
        Tile east = getEastTile(adventurer.getPlayerTile());
        Tile west = getWestTile(adventurer.getPlayerTile());
        String lookNorth = checkTile(map, north, NORTH);
        String lookSouth = checkTile(map, south, SOUTH);
        String lookEast = checkTile(map, east, EAST);
        String lookWest = checkTile(map, west, WEST);
        String askToDo = "\nWhat do you want to do?";
        String[] lookMenu = {"Look West", "Look East", "Look North", "Look South", "Look Around"};
        int chosenLookDirection = KeyboardUtility.askForChoice(askToDo, lookMenu);
        switch (chosenLookDirection) {
            case 0:
                System.out.println(lookWest);
                break;
            case 1:
                System.out.println(lookEast);
                break;
            case 2:
                System.out.println(lookNorth);
                break;
            case 3:
                System.out.println(lookSouth);
                break;
            case 4:
                System.out.println(lookWest);
                System.out.println(lookEast);
                System.out.println(lookNorth);
                System.out.println(lookSouth);
                break;
        }

        chooseMoveDirection(map,adventurer);
    }

    private static String checkTile(Map<String, List<Tile>> map, Tile tile, String direction) {
        String check = "";
        if (map.get("Enemies").contains(tile)) {
            check = "To your " + direction + " is a " + enemyGenerator.generate().getName();
        } else if (map.get("River").contains(tile)) {
            check = "To your " + direction + " is a river.";
        } else if (map.get("Items").contains(tile)) {
            check = "To your " + direction + " is an item.";
        } else if (mapFactory.isNonPlayable(tile)) {
            check = "Oops!!! The way is blocked to your " + direction;
        } else {
            check = "There way is free to your " + direction;
        }
        return check;
    }

    private static String checkTile(Map<String, List<Tile>> map, Tile tile) {
        String check = "";
        if (map.get("Enemies").contains(tile)) {
            check = "enemy";
        } else if (map.get("River").contains(tile)) {
            check = "river";
        } else if (map.get("Items").contains(tile)) {
            check = "item";
        } else if (mapFactory.isNonPlayable(tile)) {
            check = "null";
        } else {
            check ="free";
        }
        return check;
    }

    private static Tile getNorthTile(Tile tile) {
        return new Tile(tile.getX(), tile.getY() + 1);
    }

    private static Tile getSouthTile(Tile tile) {
        return new Tile(tile.getX(), tile.getY() - 1);
    }

    private static Tile getEastTile(Tile tile) {
        return new Tile(tile.getX() + 1, tile.getY());
    }

    private static Tile getWestTile(Tile tile) {
        return new Tile(tile.getX() - 1, tile.getY());
    }

    private static void checkMoveableTiles(Map<String, List<Tile>> map,Adventurer adventurer,String text,int x,int y){
        Tile movedTile;
        switch (text){
            case "enemy":
                System.out.println("Are you ready to fight. Coming soon.........");
                break;
            case "river":
                System.out.println("There is a wild foaming river in that direction. " +
                        "You jumped in and drowned while screaming for help like an old whore during her baptism!");
                break;
            case "item":
                System.out.println("There is an old chest over yonder, but it looks worthless and it's probably filled with useless crap.");
                itemService.genItem(adventurer);
                System.out.println("\n" + adventurer.getInventory());
                movedTile = adventurer.move(x,y);
                adventurer.setPlayerTile(movedTile);
                lookDirection(map, adventurer);
                break;
            case "null":
                System.out.println("OOps!!! There is Non-playable area in that direction.Choose another direction!");
            case "free":
                System.out.println("Congratulations:) you are one step ahead.");
                movedTile = adventurer.move(x,y);
                adventurer.setPlayerTile(movedTile);
                lookDirection(map,adventurer);
                break;
            default:
                break;
        }

    }
}
