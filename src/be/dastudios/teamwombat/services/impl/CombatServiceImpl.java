package be.dastudios.teamwombat.services.impl;


import be.dastudios.teamwombat.model.DiceThrow;
import be.dastudios.teamwombat.model.Dices;
import be.dastudios.teamwombat.model.adventurer.Adventurer;
import be.dastudios.teamwombat.model.adventurer.Attribute;
import be.dastudios.teamwombat.model.enemies.Enemy;

import static be.dastudios.teamwombat.util.LoadUtility.loadingDots;

public class CombatServiceImpl {
    public boolean calculateCombatInitiative(Adventurer adventurer, Enemy enemy){
        boolean adventurerHits = true;
        System.out.println("Rolling initiative");
        loadingDots();
        int adventurerDiceThrow = DiceThrow.diceThrow(Dices.D20);
        int totalAdventurerInitiativePoint = adventurerDiceThrow + adventurer.getInitiative()+
                (adventurer.getAttributes().get(Attribute.DEXTERITY)/2);
        System.out.println(adventurer.getName() + " rolls a D20");
        loadingDots();
        System.out.print(adventurerDiceThrow + "!");
        System.out.println("(D20) " + adventurerDiceThrow + "(Initiative) " + adventurer.getInitiative() +
                "(Dexterity / 2) " + (adventurer.getAttributes().get(Attribute.DEXTERITY)/2));
        loadingDots();
        System.out.print(totalAdventurerInitiativePoint + "!");
        int enemyDiceThrow = DiceThrow.diceThrow(Dices.D20);
        int totalEnemyInitiativePoint = enemyDiceThrow + enemy.getInitiative()+
                (enemy.getAttributes().get(Attribute.DEXTERITY)/2);
        System.out.println(enemy.getName() + " rolls a D20");
        loadingDots();
        System.out.print(enemyDiceThrow + "!");
        System.out.println("(D20) " + enemyDiceThrow + "(Initiative) " + enemy.getInitiative() +
                "(Dexterity / 2) " + (enemy.getAttributes().get(Attribute.DEXTERITY)/2));
        loadingDots();
        System.out.print(totalEnemyInitiativePoint + "!");
        if(totalEnemyInitiativePoint > totalAdventurerInitiativePoint)
            adventurerHits = false;

        return adventurerHits;
    }
}
