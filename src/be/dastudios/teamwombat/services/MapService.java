package be.dastudios.teamwombat.services;

import be.dastudios.teamwombat.model.adventurer.Adventurer;


public interface MapService {
    String[] getGameMaps();

    void createMap(Adventurer adventurer);
}
