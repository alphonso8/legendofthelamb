package be.dastudios.teamwombat.services;

import be.dastudios.teamwombat.model.adventurer.Adventurer;

public interface ItemService {
    int genItem(Adventurer adventurer);

}
