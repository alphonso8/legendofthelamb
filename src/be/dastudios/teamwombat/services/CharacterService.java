package be.dastudios.teamwombat.services;

import be.dastudios.teamwombat.model.adventurer.Adventurer;

public interface CharacterService {
    Adventurer createCharacter();
}
