package be.dastudios.teamwombat.services;

public interface ManagerService {
    void start();

    void showMenu(String choose);
}
