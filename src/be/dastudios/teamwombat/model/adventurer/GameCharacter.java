package be.dastudios.teamwombat.model.adventurer;

import java.util.HashMap;
import java.util.Map;

public abstract class GameCharacter {
    private String name;
    private Integer hp;
    private Integer ap;
    private Integer armourClass;
    private Integer initiative = 10;
    private Map<Attribute, Integer> attributes = new HashMap<>();

    public GameCharacter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getAp() {
        return ap;
    }

    public void setAp(Integer ap) {
        this.ap = ap;
    }

    public Integer getArmourClass() {
        return armourClass;
    }

    public void setArmourClass(Integer armourClass) {
        this.armourClass = armourClass;
    }

    public Integer getInitiative() {
        return initiative;
    }

    public void setInitiative(Integer initiative) {
        this.initiative = initiative;
    }

    public Map<Attribute, Integer> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<Attribute, Integer> attributes) {
        this.attributes = attributes;
    }

    public Integer getMaximumHp() {
        return getHp();
    }
    @Override
    public String toString() {
        return "GameCharacter{" +
                "name='" + name + '\'' +
                ", hp=" + hp +
                ", ap=" + ap +
                ", armourClass=" + armourClass +
                ", initiative=" + initiative +
                ", attributes=" + attributes +
                '}';
    }
}
