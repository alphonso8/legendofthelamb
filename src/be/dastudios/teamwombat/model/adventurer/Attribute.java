package be.dastudios.teamwombat.model.adventurer;

public enum Attribute {
    STRENGTH,
    INTELLIGENCE,
    WISDOM,
    CHARISMA,
    DEXTERITY,
    CONSTITUTION;

}
