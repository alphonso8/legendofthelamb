package be.dastudios.teamwombat.model.adventurer;

import be.dastudios.teamwombat.model.classes.CharacterClass;
import be.dastudios.teamwombat.model.items.Armours;
import be.dastudios.teamwombat.model.items.LockPicks;
import be.dastudios.teamwombat.model.items.Potion;
import be.dastudios.teamwombat.model.items.Weapons;
import be.dastudios.teamwombat.model.maps.Tile;
import be.dastudios.teamwombat.model.races.Race;
import be.dastudios.teamwombat.util.TextUtil;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Adventurer extends GameCharacter implements Serializable, Moveable {
    private final static Integer healthPower = 5;
    private Gender gender;
    private Race race;
    private CharacterClass characterClass;
    private Tile playerTile;
    private int exp;
    private Integer speed;
    private Inventory inventory;
    private Map<Attribute, Integer> attributes = new HashMap<>();

    private final static Integer value = 10;

    {
        attributes.put(Attribute.STRENGTH, value);
        attributes.put(Attribute.INTELLIGENCE, value);
        attributes.put(Attribute.WISDOM, value);
        attributes.put(Attribute.CHARISMA, value);
        attributes.put(Attribute.DEXTERITY, value);
        attributes.put(Attribute.CONSTITUTION, value);
    }


    public Adventurer(String name, Gender gender) {
        super(name);
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
        updateAttributeValueByRace();
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
        updateAttributesValueByCharacterClassBonusPoints();
    }

    public Tile getPlayerTile() {
        return playerTile;
    }

    public void setPlayerTile(Tile playerTile) {
        this.playerTile = playerTile;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Inventory getInventory() {
        inventory = new Inventory();
        if (getCharacterClass().getClassName().equals("Ranger")) {
            inventory.addItem(Weapons.YEW_WARBOW);
            inventory.addItem(Weapons.BARBED_HEAD_ARROWS);
            inventory.addItem(Armours.LEATHER_CHEST_ARMOUR);
        } else if (getCharacterClass().getClassName().equals("Rogue")) {
            inventory.addItem(Weapons.SHORT_STEEL_DAGGER);
            inventory.addItem(Armours.BLACK_HOODED_ROBES);
            inventory.addItem(LockPicks.LOCKPICKS);

        } else if (getCharacterClass().getClassName().equals("Mage")) {
            inventory.addItem(Weapons.WONKY_LOOKING_WAND);
            inventory.addItem(Armours.BURGUNDY_ROBES);
            inventory.addItem(Potion.MANA_POTION);

        } else if (getCharacterClass().getClassName().equals("Warrior")) {
            inventory.addItem(Weapons.CASTLE_FORGED_ARMING_SWORD);
            inventory.addItem(Armours.KITE_SHIELD);
            inventory.addItem(Armours.ROYAL_BLUE_BRIGANDINE_GAMBESON);
        }
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Map<Attribute, Integer> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<Attribute, Integer> attributes) {
        this.attributes = attributes;
    }

    @Override
    public Integer getHp() {
        return attributes.get(Attribute.CONSTITUTION) * healthPower;
    }

    @Override
    public String toString() {

        String name = getName().toUpperCase();
        String gender = "Gender: " + getGender();
        String race = "Race: " + getRace().getName();
        String CharacterClass = "Class: " + getCharacterClass().getClassName();
        String exp = "Experience Points(XP) : " + getExp() + "/99";
        String initiative = "Initiative: " + getInitiative();
        String hp = "Health Points (HP): " + getHp() + "/" + getMaximumHp();
        StringBuilder attributeNames = new StringBuilder();
        StringBuilder attributePoints = new StringBuilder();
        for (Attribute attribute : getAttributes().keySet()) {
            attributeNames.append(attribute.toString()).append("   ");
            attributePoints.append(getAttributes().get(attribute)).append("           ");
        }

        return TextUtil.generateLine('#', 75)
                + "\n"
                + name
                + "\n"
                + gender
                + "\n"
                + race
                + "\n"
                + CharacterClass
                + "\n"
                + exp
                + "\n"
                + hp
                + "\n"
                + initiative
                + "\n"
                + attributeNames
                + "\n"
                + attributePoints
                + "\n"
                + TextUtil.generateLine('#', 75);
    }

    @Override
    public Integer getInitiative() {
        return calcInitiative();
    }

    public Integer calcInitiative() {
        int result = super.getInitiative();
        if (getCharacterClass().getClassName().equals("Rogue") || (getCharacterClass().getClassName().equals("Ranger"))) {
            result = super.getInitiative() + 1;
        }
        return result;
    }

    @Override
    public Tile move(int x, int y) {

        playerTile.setX(playerTile.getX() + x);
        playerTile.setY(playerTile.getY() + y);
        return playerTile;


    }

    private void updateAttributeValueByRace() {

        for (Attribute attributeOfAdventurer : attributes.keySet()) {
            int attributeValue = attributes.get(attributeOfAdventurer);
            for (Attribute attributeOfRace : getRace().getRaceAttributes()) {
                if (attributeOfAdventurer.equals(attributeOfRace)) {
                    attributeValue += getRace().getBonusPoints();
                }
            }
            attributes.put(attributeOfAdventurer, attributeValue);
        }
    }

    private void updateAttributesValueByCharacterClassBonusPoints() {
        for (Attribute attributeAdventurer : attributes.keySet()) {
            int attributesValue = attributes.get(attributeAdventurer);
            for (Attribute attribute : getCharacterClass().getBonus().keySet()) {
                if (attributeAdventurer.equals(attribute)) {
                    attributesValue += getCharacterClass().getBonus().get(attribute);
                }
            }
            attributes.put(attributeAdventurer, attributesValue);
        }
    }

}
