package be.dastudios.teamwombat.model.adventurer;

import be.dastudios.teamwombat.model.maps.Tile;


public interface Moveable {
    Tile move(int x, int y);
}
