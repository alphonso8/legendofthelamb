package be.dastudios.teamwombat.model.adventurer;

import be.dastudios.teamwombat.model.items.Item;
import be.dastudios.teamwombat.util.KeyboardUtility;
import java.util.ArrayList;
import java.util.List;


public class Inventory {

    private List<Item> inventorySlots;
    private int maxWeightInventory = 15;
    private int weightInventory;

    public Inventory() {
        this.inventorySlots = new ArrayList<>();
        for (Item item : inventorySlots) {
            weightInventory += item.getWeightOfItem();
        }
    }

    public void addItem(Item item) {
        if (weightInventory + item.getWeightOfItem() <= maxWeightInventory) {
            inventorySlots.add(item);
            weightInventory = weightInventory + item.getWeightOfItem();
        } else {
            if (KeyboardUtility.askYOrN("Inventory is full. Do you want replace this item with another from your inventory?")) {
               if (showItemList()== 1)
                   addItem(item);
            }
        }
    }

    private int showItemList() {
        String message = "Which item do you want to discard?";
        String[] itemOptions = (String[]) inventorySlots.stream().map(Item::getItemInfo).toArray();
        int chosenItemIdx = KeyboardUtility.askForChoice(message, itemOptions);
        if (inventorySlots.size() > 0) {
            return removeItemFromList(chosenItemIdx);
        }
        return 0;
    }


        private int removeItemFromList ( int chosenItemIdx){
            Item removedItem = inventorySlots.remove(chosenItemIdx);
            weightInventory -= removedItem.getWeightOfItem();
            return 1;
        }

    public List<Item> getInventorySlots() {
        return inventorySlots;
    }

    public void setInventorySlots(List<Item> inventorySlots) {
        this.inventorySlots = inventorySlots;
    }

    public int getMaxWeightInventory() {
        return maxWeightInventory;
    }


    public int getWeightInventory() {
        return weightInventory;
    }

    public void setWeightInventory(int weightInventory) {
        this.weightInventory = weightInventory;
    }

    @Override
    public String toString() {
        return "Your inventory{" + inventorySlots +
                ", maxWeightInventory=" + maxWeightInventory +
                ", weightInventory=" + weightInventory +
                '}';
    }
}
