package be.dastudios.teamwombat.model.adventurer;

public class Location {
    private int xpos;
    private int ypos;

    public Location(int xpos, int ypos) {
        this.xpos = xpos;
        this.ypos = ypos;
    }
}
