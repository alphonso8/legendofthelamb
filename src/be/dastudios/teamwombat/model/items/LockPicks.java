package be.dastudios.teamwombat.model.items;


public enum LockPicks implements Item {
    LOCKPICKS(1, "Lockpicks");
    private final String name;


    private final int weight;

    LockPicks(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int getWeightOfItem() {
        return weight;
    }

    @Override
    public int getHitChanceOfItem() {
        return 0;
    }

    @Override
    public int getDamageOfItem() {
        return 0;
    }

    @Override
    public int getArmourBaseOfItem() {
        return 0;
    }

    @Override
    public int getACOfItem() {
        return 0;
    }

    @Override
    public String getItemInfo() {
        return "Item: " + getName() + " weight in at " + getWeight()+"kg";
    }


}
