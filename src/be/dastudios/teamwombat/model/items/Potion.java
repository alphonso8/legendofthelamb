package be.dastudios.teamwombat.model.items;


public enum Potion implements Item {
    MANA_POTION(1, "Mana Potion"),
    HEALTH_POTION(1,"Health Potion"),
    STAMINA_POTION(1,"Stamina Potion");

    private final int weight;
    private final String name;

    Potion(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }


    @Override
    public int getWeightOfItem() {
        return 0;
    }

    @Override
    public int getHitChanceOfItem() {
        return 0;
    }

    @Override
    public int getDamageOfItem() {
        return 0;
    }

    @Override
    public int getArmourBaseOfItem() {
        return 0;
    }

    @Override
    public int getACOfItem() {
        return 0;
    }

    @Override
    public String getItemInfo() {
        return "Item: " + getName() + " weight in at " + getWeight() + "kg";
    }
}
