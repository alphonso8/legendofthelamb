package be.dastudios.teamwombat.model.items;

import static be.dastudios.teamwombat.model.DiceThrow.*;
import static be.dastudios.teamwombat.model.Dices.*;

public enum Weapons implements Item {
    CASTLE_FORGED_ARMING_SWORD(4, diceThrow(D20),diceThrow(D10),"Castle Forged Arming Sword"),
    WONKY_LOOKING_WAND(1,diceThrow(D20),diceThrow(D2),"Wonky Looking Wand"),
    YEW_WARBOW(2,diceThrow(D20),diceThrow(D12), "Yew Warbow"),
    BARBED_HEAD_ARROWS(1, diceThrow(D20),diceThrow(D2), "Barbed Head Arrows"),
    SHORT_STEEL_DAGGER(2,diceThrow(D20),diceThrow(D4), "Short Steel Dagger");


    private final int weight;
    private final int hitChance;
    private final int damage;
    private final String name;


    Weapons(int weight, int hitChance, int damage, String name) {
        this.weight = weight;
        this.hitChance = hitChance;
        this.damage = damage;
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public int getHitChance() {
        return hitChance;
    }

    public int getDamage() {
        return damage;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getWeightOfItem() {
        return weight;
    }

    @Override
    public int getHitChanceOfItem() {
        return hitChance;
    }

    @Override
    public int getDamageOfItem() {
        return damage;
    }

    @Override
    public int getArmourBaseOfItem() {
        return 0;
    }

    @Override
    public int getACOfItem() {
        return 0;
    }

    @Override
    public String getItemInfo() {
        return "Item: " + getName() + "  weight in at " + getWeight() + "kg";
    }
}
