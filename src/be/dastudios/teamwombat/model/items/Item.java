package be.dastudios.teamwombat.model.items;

import java.io.Serializable;

public interface Item extends Serializable {

    int getWeightOfItem();
    int getHitChanceOfItem();
    int getDamageOfItem();
    int getArmourBaseOfItem();
    int getACOfItem();
    String getItemInfo();


}
