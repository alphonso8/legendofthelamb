package be.dastudios.teamwombat.model.items;

public enum Armours implements Item{
    KITE_SHIELD(5,10,10, "Kite Shield"),
    ROYAL_BLUE_BRIGANDINE_GAMBESON(3,6,6, "Royal Blue Brigandine Gambeson"),
    BURGUNDY_ROBES(1,4,4, "Burgundy Robes"),
    LEATHER_CHEST_ARMOUR(2,8,8, "leather Chest Armour"),
    BLACK_HOODED_ROBES(1,4,4, "Black Hooded Robes");


    private final int weight;
    private final int armourBase;
    private final int AC;
    private final String name;

    Armours(int weight, int armourBase, int AC, String name) {
        this.weight = weight;
        this.armourBase = armourBase;
        this.AC = AC;
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public int getArmourBase() {
        return armourBase;
    }

    public int getAC() {
        return AC;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getWeightOfItem() {
        return weight;
    }

    @Override
    public int getHitChanceOfItem() {
        return 0;
    }

    @Override
    public int getDamageOfItem() {
        return 0;
    }

    @Override
    public int getArmourBaseOfItem() {
        return armourBase;
    }

    @Override
    public int getACOfItem() {
        return AC;
    }

    @Override
    public String getItemInfo() {
        return "Item: " + getName() + " weight in at " + getWeight()+"kg";
    }


}
