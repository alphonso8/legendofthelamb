package be.dastudios.teamwombat.model.maps;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class MapFactory {
    private String map1Name = Map.MYSTERIOUS_FOREST.getName();
    private String map2Name = Map.NECROPOLIS.getName();
    private java.util.Map<String, List<Tile>> map1 = new HashMap<>();

    public java.util.Map<String, List<Tile>> createMap1(){
        map1.put("Enemies",createEnemyTiles());
        map1.put("River",createRiverTiles());
        map1.put("Items",createItemTiles());

        return map1;
    }


    public static List<Tile> createRiverTiles(){
        return Arrays.asList(new Tile(4,0),
                    new Tile(4,1),
                    new Tile(4,2),
                    new Tile(4,3),
                    new Tile(4,4),
                    new Tile(4,6),
                    new Tile(4,7),
                    new Tile(4,8),
                    new Tile(4,9),
                    new Tile(4,10));

    }

    public static List<Tile> createEnemyTiles(){
        return Arrays.asList(new Tile(2,2),
                new Tile(5,4),
                new Tile(2,11),
                new Tile(6,12),
                new Tile(10,12),
                new Tile(14,9),
                new Tile(0,9),
                new Tile(12,3));
    }

    public static List<Tile> createItemTiles(){
        return Arrays.asList(new Tile(3,5),
                            new Tile(4,12),
                            new Tile(8,12));
    }

    public boolean isNonPlayable(Tile tile){

        return (tile.getY() - 1 < 0) || (tile.getY() + 1 == 15) || (tile.getX() - 1 < 0) || (tile.getX() + 1 == 15);

    }

    public String[] getMaps(){
        String[] gameMapNames = {map1Name,map2Name};
        return gameMapNames;
    }




}
