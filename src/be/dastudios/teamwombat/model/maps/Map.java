package be.dastudios.teamwombat.model.maps;

public enum Map {
    MYSTERIOUS_FOREST("Mysterious Forest"),
    NECROPOLIS("Necropolis");

    private String name;

    Map(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
