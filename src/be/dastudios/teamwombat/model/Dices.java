package be.dastudios.teamwombat.model;


public enum Dices {
    D2(2),
    D4(4),
    D6(6),
    D8(8),
    D10(10),
    D12(12),
    D20(20),
    D100(100);

    private final int maxThrow;

    Dices(int maxThrow) {
        this.maxThrow = maxThrow;

    }

    public int getMaxThrow() {
        return maxThrow;
    }

}
