package be.dastudios.teamwombat.model.races;

import be.dastudios.teamwombat.model.adventurer.Attribute;

import java.util.List;

public abstract class Race {
    private String name;
    private int bonusPoints;
    private List<Attribute> raceAttributes;

    public Race() {
    }

    public Race(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(int bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public List<Attribute> getRaceAttributes() {
        return raceAttributes;
    }

    public void setRaceAttributes(List<Attribute> raceAttributes) {
        this.raceAttributes = raceAttributes;
    }

    public static Race getRaceByName(String name){
        Race race = null;
        switch(name){
            case "Elf":
                race = new Elf();
                break;
            case "Human":
                race = new Human();
                break;
            case "Dwarf":
                race = new Dwarf();
                break;

            default:
                break;
        }

        return race;
    }


}
