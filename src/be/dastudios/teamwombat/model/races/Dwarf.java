package be.dastudios.teamwombat.model.races;

import be.dastudios.teamwombat.model.adventurer.Attribute;

import java.util.Arrays;
import java.util.List;

import static be.dastudios.teamwombat.model.adventurer.Attribute.*;


public class Dwarf extends Race {




    @Override
    public String getName() {
        return "Dwarf";
    }

    @Override
    public int getBonusPoints() {
        return 3;
    }

    @Override
    public List<Attribute> getRaceAttributes() {
        return Arrays.asList(CONSTITUTION,STRENGTH);
    }
}
