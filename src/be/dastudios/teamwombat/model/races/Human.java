package be.dastudios.teamwombat.model.races;

import be.dastudios.teamwombat.model.adventurer.Attribute;

import static be.dastudios.teamwombat.model.adventurer.Attribute.*;

import java.util.Arrays;
import java.util.List;

public class Human extends Race {


    @Override
    public String getName() {
        return "Human";
    }

    @Override
    public int getBonusPoints() {
        return 1;
    }

    @Override
    public List<Attribute> getRaceAttributes() {
        return Arrays.asList(CHARISMA,CONSTITUTION,DEXTERITY,WISDOM,INTELLIGENCE,STRENGTH);
    }
}
