package be.dastudios.teamwombat.model.enemies;

import be.dastudios.teamwombat.model.adventurer.Attribute;
import java.util.*;

public class EnemyGenerator {
    private static Random random = new Random();

    public static Enemy generate(){
        String name = genRandomEnemyName();
        Enemy enemy = new Enemy(name);
        enemy.setHp(genRandomEnemyValue(100));
        enemy.setAp(genRandomEnemyValue(100));
        enemy.setArmourClass(genRandomEnemyValue(25));
        enemy.setInitiative(genRandomEnemyValue(10));
        enemy.setAttributes(genRandomEnemyAttributes());

        return enemy;
    }

    private static Map<Attribute, Integer> genRandomEnemyAttributes() {
        Map<Attribute, Integer> enemyAttributes = new HashMap<>();
        int value = genRandomEnemyValue(10);
        enemyAttributes.put(Attribute.STRENGTH, value);
        enemyAttributes.put(Attribute.INTELLIGENCE, value);
        enemyAttributes.put(Attribute.WISDOM, value);
        enemyAttributes.put(Attribute.CHARISMA, value);
        enemyAttributes.put(Attribute.DEXTERITY, value);
        enemyAttributes.put(Attribute.CONSTITUTION, value);
        return enemyAttributes;
    }

    private static int genRandomEnemyValue(int value) {

        return random.nextInt(value);
    }

    public static String genRandomEnemyName(){
        List<String> nameList = enemyNameList();
        return nameList.get(random.nextInt(nameList.size()));

    }

    public static List<String> enemyNameList(){
        return Arrays.asList("Zombie","Skeleton","Goblin","GiantSpider",
                "Werewolf","Mummy","Vampire","Monster","Snake");
    }
}
