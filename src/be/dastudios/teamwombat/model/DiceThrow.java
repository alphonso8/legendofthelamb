package be.dastudios.teamwombat.model;

import static be.dastudios.teamwombat.util.RandomUtility.RANDOM_NUM_GEN;


public class DiceThrow {

    public static int diceThrow(Dices dices) {

        return RANDOM_NUM_GEN.nextInt(dices.getMaxThrow()+1);

    }
}
