package be.dastudios.teamwombat.model.classes;

import be.dastudios.teamwombat.model.adventurer.Attribute;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Warrior extends CharacterClass {

    private static final int classBonusOfStrength = 3;
    private static final int classBonusOfConstitution= 2;

    private List<Skills> skillsList;
    private Integer StaminaPoints;

    public Warrior() {
        this.skillsList = Stream.of(Skills.SHIELD_BASH).collect(Collectors.toList());
        StaminaPoints = 50;
    }

    public List<Skills> getSkillsList() {
        return skillsList;
    }

    public void setSkillsList(List<Skills> skillsList) {
        this.skillsList = skillsList;
    }

    public Integer getStaminaPoints() {
        return StaminaPoints;
    }

    public void setStaminaPoints(Integer staminaPoints) {
        StaminaPoints = staminaPoints;
    }

    @Override
    public String getClassName() {
        return "Warrior";
    }

    @Override
    public Map<Attribute, Integer> getBonus() {

        Map<Attribute, Integer> classes =super.getBonus();
        classes.put(Attribute.CONSTITUTION,classBonusOfConstitution);
        classes.put(Attribute.STRENGTH,classBonusOfStrength);
        return classes;
    }
}
