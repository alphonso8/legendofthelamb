package be.dastudios.teamwombat.model.classes;

import be.dastudios.teamwombat.model.adventurer.Attribute;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Mage extends CharacterClass{

    private static final int classBonusOfWisdom = 3;
    private static final int classBonusOfIntelligence= 2;

    private List<Spell> spellList;
    private Integer manaPoints;


    public Mage() {
        this.spellList = Stream.of(Spell.LIGHTNING_BOLT).collect(Collectors.toList());
        this.manaPoints = 50;
    }

    public List<Spell> getSpellList() {
        return spellList;
    }

    public void setSpellList(List<Spell> spellList) {
        this.spellList = spellList;
    }

    public Integer getManaPoints() {
        return manaPoints;
    }

    public void setManaPoints(Integer manaPoints) {
        this.manaPoints = manaPoints;
    }

    @Override
    public String getClassName() {
        return "Mage";
    }

    @Override
    public Map<Attribute, Integer> getBonus() {

        Map<Attribute, Integer> classes =super.getBonus();
        classes.put(Attribute.WISDOM,classBonusOfWisdom);
        classes.put(Attribute.INTELLIGENCE,classBonusOfIntelligence);
        return classes;
    }
}
