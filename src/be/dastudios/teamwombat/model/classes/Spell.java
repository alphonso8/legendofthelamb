package be.dastudios.teamwombat.model.classes;

public enum Spell {
    LIGHTNING_BOLT,
    FIRE_BALL,
    SPIRIT_BOMB,
    GOLD_STRIKE
}
