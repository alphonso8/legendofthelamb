package be.dastudios.teamwombat.model.classes;

public enum Skills {
    PIERCING_SHOT,
    BACK_STAB,
    STEALTH,
    SHIELD_BASH
    }
