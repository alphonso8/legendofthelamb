package be.dastudios.teamwombat.model.classes;

import be.dastudios.teamwombat.model.adventurer.Attribute;
import be.dastudios.teamwombat.model.items.Item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CharacterClass {

    private Map<Attribute, Integer> bonus = new HashMap<>();
    private List<Item> itemList;

    public Map<Attribute, Integer> getBonus() {
        return bonus;
    }

    public void setBonus(Map<Attribute, Integer> bonus) {
        this.bonus = bonus;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public abstract String getClassName();

    public static CharacterClass getCharacterClassByName(String name){
        CharacterClass characterClass = null;

        switch(name){
            case "Warrior":
                characterClass = new Warrior();
                break;
            case "Mage":
                characterClass = new Mage();
                break;
            case "Rogue":
                characterClass= new Rogue();
                break;
            case "Ranger":
                characterClass= new Ranger();
                break;

            default:
                break;
        }

        return characterClass;

    }

}
