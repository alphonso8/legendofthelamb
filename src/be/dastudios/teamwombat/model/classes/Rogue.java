package be.dastudios.teamwombat.model.classes;

import be.dastudios.teamwombat.model.adventurer.Attribute;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Rogue extends CharacterClass {

    private static final int classBonusOfDexterity = 3;
    private static final int classBonusOfIntelligence = 1;
    private static final int classBonusOfIniative = 1;

    private List<Skills> skillsList;
    private Integer StaminaPoints;

    public Rogue() {
        this.skillsList = Stream.of(Skills.STEALTH, Skills.BACK_STAB).collect(Collectors.toList());
        StaminaPoints = 50;
    }

    public List<Skills> getSkillsList() {
        return skillsList;
    }

    public void setSkillsList(List<Skills> skillsList) {
        this.skillsList = skillsList;
    }

    public Integer getStaminaPoints() {
        return StaminaPoints;
    }

    public void setStaminaPoints(Integer staminaPoints) {
        StaminaPoints = staminaPoints;
    }

    @Override
    public Map<Attribute, Integer> getBonus() {
        Map<Attribute, Integer> classes =super.getBonus();
        classes.put(Attribute.DEXTERITY,classBonusOfDexterity);
        classes.put(Attribute.INTELLIGENCE,classBonusOfIntelligence);
        return classes;
    }

    @Override
    public String getClassName() {
        return "Rogue";
    }
}
