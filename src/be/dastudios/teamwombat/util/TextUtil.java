package be.dastudios.teamwombat.util;

public final class TextUtil {

    public static void printTitle(String titel) {
        String lijn = generateLine('♥', titel.length());
        String titleNice = String.format("%s%n%s%n%s", lijn, titel, lijn);
        System.out.println(titleNice);
    }

    public static void printSubheading(String titel) {
        String lijn = generateLine('◘', titel.length());
        String titleNice = String.format("%s%n%s%n%s", lijn, titel, lijn);
        System.out.println(titleNice);
    }

    public static String generateLine(char c, int length) {
        StringBuilder lijnAsSB = new StringBuilder();
        for (int i = 0; i < length; i++) {
            lijnAsSB.append(c);
        }
        return lijnAsSB.toString();
    }
}

