package be.dastudios.teamwombat.util;


import java.util.List;
import java.util.Scanner;

public abstract class KeyboardUtility {

    public static final Scanner KEYBOARD = new Scanner(System.in);
    private static final String INVALID_MSG = "Invalid input! Please try again...";

    public static int askForInt(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static boolean askYOrN(String message) {
        while (true) {
            String input = ask(message + "(y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
                continue;
            }
            switch (firstLetter) {
                case 'y':
                    return true;
                case 'n':
                    return false;
                default:
                    break;
            }
        }
    }

    public static String ask(String message) {
        System.out.println(message);
        return KEYBOARD.nextLine();
    }

//    public static double askForDouble(String message) {
//        while (true) {
//            String input = ask(message);
//            try {
//                return Double.parseDouble(input);
//            } catch (Exception e) {
//                System.out.println(INVALID_MSG);
//            }
//        }


    public static int askForChoice(String message, String[] options) {
        System.out.println(message);
        return askForChoice(options);
    }

    public static int askForChoice(String[] options) {
        while (true) {
            for (int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }
            int chosenIdx = askForInt(String.format("Enter your choice (1-%d):", options.length)) - 1;
            if (chosenIdx < 0 || chosenIdx >= options.length) {
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a choice in the valid range");
            } else {
                return chosenIdx;
            }
        }
    }


    public static String askForChoice(List<String> options, List<String> descriptions) {
        while (true) {
            for (int i = 0; i < options.size(); i++) {
                System.out.printf("%s- %s%n", options.get(i), descriptions.get(i));
            }
            String chosenOptions = ask("What do you want to do?");
            if (!options.contains(chosenOptions)) {
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a valid choice");
            } else {
                return chosenOptions;
            }
        }

    }
        public static String askForChoice(String message, List < String > options){
        while (true) {
            System.out.println(message);
                for (int i = 0; i < options.size(); i++) {
                    System.out.printf("%s%n", options.get(i));
                }
            String chosenOptions = KEYBOARD.next();
                if (!options.contains(chosenOptions)) {
                    System.out.println(INVALID_MSG);
                    System.out.println("Please enter a valid choice");
                } else {
                    return chosenOptions;
                }
            }

        }
    }



