package be.dastudios.teamwombat.main;

import be.dastudios.teamwombat.services.ManagerService;
import be.dastudios.teamwombat.services.impl.ManagerServiceImpl;

public class MainMenu {
    public static void main(String[] args) {

        ManagerService service = new ManagerServiceImpl();
        service.start();

    }
}
